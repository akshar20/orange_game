//
//  GameScene.swift
//  MyGame
//
//  Created by MacStudent on 2019-02-20.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var orange:Orange?
    
    override func didMove(to view: SKView) {
        
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let touch = touches.first else{
            return
        }

        
        let mouseLocation = touch.location(in: self)
        
        print("Mouse At: \(mouseLocation)")
    
        
        
        // detect what sprite was touched
        let spriteTouched = self.atPoint(mouseLocation)
        
        if (spriteTouched.name == "tree") {
            print("YOU CLICKED THE TREE")
            
            self.orange = Orange()
            orange!.position = mouseLocation
            addChild(orange!)
            
            
            // PLAY SOUND
            self.orange?.run(SKAction.playSoundFileNamed("throw.mp3", waitForCompletion: false))
        
            // throw the orange
            let direction = CGVector(dx: 40, dy: 50)
            orange?.physicsBody?.applyImpulse(direction)
            
            
            

        }
        else {
            print("YOU CLICKED SOMETHING ELSE: \(spriteTouched.name!)")
        }

    }
    
}
